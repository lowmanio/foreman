# local imports
import base_tester
from foreman.model import User, UserCaseRoles, UserTaskRoles, ForemanOptions
from foreman.utils.utils import session


class ModelTestUserBase(base_tester.UnitTestCase):
    defaults = ForemanOptions.get_options()


class UserWriteTestCase(ModelTestUserBase):
    def setUp(self):
        self.new_user = User("username", "password", "forename", "surname", "email@email.com")
        session.add(self.new_user)
        session.commit()

    def tearDown(self):
        session.delete(self.new_user)

    def test_new_user_test_case(self):
        self.assertEqual(User.get_number_unvalidated(), 1)
        self.assertEqual(self.new_user.validated, False)
        self.new_user.validated = True
        session.commit()

        self.assertEqual(User.get_number_unvalidated(), 0)

        self.assertEquals(self.new_user.active, True)
        self.new_user.deactivate()
        self.assertEquals(self.new_user.active, False)

        self.assertEquals(len(self.new_user.users_roles()), 0)

        self.assertTrue(User.check_password("username", "password"))

        self.assertTrue(User.check_password(self.new_user.username, "password"))  # right password
        self.assertEquals(self.new_user.locked_out, False)
        self.defaults.number_logins_before_account_lockout = 1
        self.assertFalse(User.check_password(self.new_user.username, "nottherightpassword")) # wrong password, get lock out
        self.assertEquals(self.new_user.locked_out, False) # one false password ok
        self.assertFalse(User.check_password(self.new_user.username, "nottherightpassword")) # wrong password, get lock out
        self.assertEquals(self.new_user.locked_out, True) # more than one not ok
        self.assertFalse(User.check_password(self.new_user.username, "password")) # right password, but locked out
        self.defaults.number_logins_before_account_lockout = None
        self.assertTrue(User.check_password(self.new_user.username, "password"))  # right password


class UserReadTestCase(ModelTestUserBase):
    def setUp(self):
        self.admin = User.get(1)
        self.inv = User.get(3)
        self.inv2 = User.get(2)
        self.case = User.get(20)

    def test_read_user_test_case(self):
        self.assertEquals(self.admin.forename, "The")
        self.assertEquals(self.admin.surname, "Administrator")
        self.assertEquals(self.admin.middle, None)
        self.assertEquals(self.admin.username, "administrator")
        self.assertEquals(self.admin.email, "example@example.org")
        self.assertEquals(self.admin.validated, True)
        self.assertEquals(self.admin.telephone, None)
        self.assertEquals(self.admin.job_title, "Administrator")
        self.assertEquals(self.admin.photo, "default.png")
        self.assertEquals(self.admin.active, True)
        self.assertEquals(self.admin.department, "IT Security")
        self.assertEquals(self.admin.team.id, 1)
        self.assertEquals(self.admin.fullname, "The Administrator")

        self.assertEquals(self.inv2.manager.id, 1)
        self.assertEquals(self.inv.is_a_manager(), False)

        self.assertEquals(self.admin.manager, None)
        self.assertEquals(self.admin.is_a_manager(), True)

        self.assertEquals(self.admin.is_manager_of(self.inv2), True)
        self.assertEquals(self.inv.is_manager_of(self.admin), False)
        self.assertEquals(self.inv.is_manager_of(self.inv), False)

        self.assertEquals(len(self.inv.all_reports()), 0)

    def test_user_roles_test_case(self):
        self.assertEquals(len(self.admin.users_roles()), 1)
        self.assertEquals(len(self.inv.users_roles()), 2)

        self.assertTrue(self.admin.is_investigator())
        self.assertTrue(self.admin.is_QA())
        self.assertTrue(self.admin.is_case_manager())
        self.assertTrue(self.admin.is_requester())
        self.assertTrue(self.admin.is_worker())
        self.assertTrue(self.admin.is_examiner())
        self.assertTrue(self.admin.is_admin())
        self.assertTrue(self.admin.is_authoriser())

        self.assertTrue(self.inv.is_investigator())
        self.assertTrue(self.inv.is_QA())
        self.assertFalse(self.inv.is_case_manager())
        self.assertFalse(self.inv.is_requester())
        self.assertTrue(self.inv.is_worker())
        self.assertTrue(self.inv.is_examiner())
        self.assertFalse(self.inv.is_admin())
        self.assertFalse(self.inv.is_authoriser())

    def test_static_user_methods_test_test(self):
        self.assertEquals(User.is_valid_id(2), True)
        self.assertEquals(User.is_valid_id(200), False)

        self.assertEqual(User.get_user_with_username(self.inv.username), self.inv)
        self.assertIsNone(User.get_user_with_username("non_existant"))

        self.assertEqual(User.get_user_with_role(UserCaseRoles.PRINCIPLE_CASE_MANAGER, case_id=4), self.case)
        self.assertEqual(User.get_user_with_role(UserTaskRoles.PRINCIPLE_QA, task_id=4), self.inv2)
