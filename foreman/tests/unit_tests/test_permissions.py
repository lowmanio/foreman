# local imports
import base_tester
from foreman.model.permissions import *
from foreman.model import User, Case, Task, Evidence


class ModelPermissionBase(base_tester.UnitTestCase):
    @staticmethod
    def set_trues(objs, true_list):
        obj_trues = []
        obj_falses = []
        for i in range(0, len(objs)-1):
            if i in true_list:
                obj_trues.append(objs[i])
            else:
                obj_falses.append(objs[i])
        return obj_trues, obj_falses

    def user_asserts(self, checker, user_list, obj, true_assert, mult_checker=None):
        for user in user_list:
            if true_assert:
                if mult_checker is None:
                    self.assertTrue(checker().check(user, obj))
                else:
                    self.assertTrue(checker(*mult_checker).check(user, obj))
            else:
                if mult_checker is None:
                    self.assertFalse(checker().check(user, obj))
                else:
                    self.assertFalse(checker(*mult_checker).check(user, obj))

    def obj_asserts(self, checker, obj_list, user, true_assert, mult_checker=None):
        for ol in obj_list:
            if true_assert:
                if mult_checker is None:
                    self.assertTrue(checker().check(user, ol))
                else:
                    self.assertTrue(checker(*mult_checker).check(user, ol))
            else:
                if mult_checker is None:
                    self.assertFalse(checker().check(user, ol))
                else:
                    self.assertFalse(checker(*mult_checker).check(user, ol))


class CheckersTestCase(ModelPermissionBase):
    def setUp(self):
        self.users = [User.get(1), User.get(2), User.get(3), User.get(4), User.get(5), User.get(6), User.get(7),
                      User.get(8), User.get(9), User.get(10), User.get(11), User.get(12), User.get(13), User.get(14),
                      User.get(15), User.get(16), User.get(17), User.get(18), User.get(19), User.get(20), User.get(21),
                      User.get(22), User.get(23), User.get(24), User.get(25), User.get(26), User.get(27), User.get(28),
                      User.get(29), User.get(30), User.get(31), User.get(32), User.get(33), User.get(34), User.get(35),
                      User.get(36), User.get(37), User.get(38), User.get(39), User.get(40)]

        self.cases = [Case.get(1), Case.get(2), Case.get(3), Case.get(4), Case.get(5), Case.get(6), Case.get(7),
                      Case.get(8), Case.get(9), Case.get(10), Case.get(11), Case.get(12), Case.get(13)]

        self.tasks = [Task.get(1), Task.get(2), Task.get(3), Task.get(4), Task.get(5), Task.get(6), Task.get(7),
                      Task.get(8), Task.get(9), Task.get(10), Task.get(11), Task.get(12), Task.get(13), Task.get(14),
                      Task.get(15), Task.get(16), Task.get(17), Task.get(18), Task.get(19), Task.get(20), Task.get(21),
                      Task.get(22), Task.get(23), Task.get(24), Task.get(25), Task.get(26), Task.get(27), Task.get(28),
                      Task.get(29), Task.get(30), Task.get(31), Task.get(32), Task.get(33), Task.get(34), Task.get(35),
                      Task.get(36), Task.get(37), Task.get(38), Task.get(39), Task.get(40), Task.get(41), Task.get(42),
                      Task.get(43), Task.get(44), Task.get(45), Task.get(46), Task.get(47)]

        self.evidence = [Evidence.get(1), Evidence.get(2), Evidence.get(3), Evidence.get(4), Evidence.get(5),
                         Evidence.get(6), Evidence.get(7), Evidence.get(8), Evidence.get(9), Evidence.get(10),
                         Evidence.get(11), Evidence.get(12)]

    def test_UserIsCurrentUserChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [0])
        self.user_asserts(UserIsCurrentUserChecker, user_trues, self.users[0], True)
        self.user_asserts(UserIsCurrentUserChecker, user_falses, self.users[0], False)

    def test_AdminChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [0])
        self.user_asserts(AdminChecker, user_trues, None, True)
        self.user_asserts(AdminChecker, user_falses, None, False)

    def test_CaseManagerForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [16, 17])
        self.user_asserts(CaseManagerForCaseChecker, user_trues, Case.get(1), True)
        self.user_asserts(CaseManagerForCaseChecker, user_falses, Case.get(1), False)

    def test_PrimaryCaseManagerForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [16])
        self.user_asserts(PrimaryCaseManagerForCaseChecker, user_trues, Case.get(1), True)
        self.user_asserts(PrimaryCaseManagerForCaseChecker, user_falses, Case.get(1), False)

    def test_CaseManagerForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [17])
        self.user_asserts(CaseManagerForTaskChecker, user_trues, Task.get(5), True)
        self.user_asserts(CaseManagerForTaskChecker, user_falses, Task.get(5), False)

    def test_CaseManagerChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [16, 17, 18, 19, 20, 21, 22, 23, 24, 25])
        self.user_asserts(CaseManagerChecker, user_trues, None, True)
        self.user_asserts(CaseManagerChecker, user_falses, None, False)

    def test_InvestigatorForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [3])
        self.user_asserts(InvestigatorForTaskChecker, user_trues, Task.get(25), True)
        self.user_asserts(InvestigatorForTaskChecker, user_falses, Task.get(25), False)

    def test_CaseManagerForEvidenceChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [21])
        self.user_asserts(CaseManagerForEvidenceChecker, user_trues, Evidence.get(5), True)
        self.user_asserts(CaseManagerForEvidenceChecker, user_falses, Evidence.get(5), False)

    def test_AssignSelfTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        self.user_asserts(AssignSelfTaskChecker, user_trues, Task.get(18), True)
        self.user_asserts(AssignSelfTaskChecker, user_falses, Task.get(18), False)

    def test_CompleteInvestigationForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [3])
        self.user_asserts(CompleteInvestigationForTaskChecker, user_trues, Task.get(20), True)
        self.user_asserts(CompleteInvestigationForTaskChecker, user_falses, Task.get(20), False)

    def test_InvestigatorForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4])
        self.user_asserts(InvestigatorForCaseChecker, user_trues, Case.get(4), True)
        self.user_asserts(InvestigatorForCaseChecker, user_falses, Case.get(4), False)

    def test_InvestigatorForEvidenceChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4, 5, 6, 7, 8])
        self.user_asserts(InvestigatorForEvidenceChecker, user_trues, Evidence.get(8), True)
        self.user_asserts(InvestigatorForEvidenceChecker, user_falses, Evidence.get(8), False)

    def test_InvestigatorChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        self.user_asserts(InvestigatorChecker, user_trues, None, True)
        self.user_asserts(InvestigatorChecker, user_falses, None, False)

    def test_QAForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [2, 4])
        self.user_asserts(QAForTaskChecker, user_trues, Task.get(13), True)
        self.user_asserts(QAForTaskChecker, user_falses, Task.get(13), False)

    def test_CompleteQAForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1])
        self.user_asserts(CompleteQAForTaskChecker, user_trues, Task.get(8), True)
        self.user_asserts(CompleteQAForTaskChecker, user_falses, Task.get(8), False)

    def test_AddNotesForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [3, 4])
        self.user_asserts(AddNotesForTaskChecker, user_trues, Task.get(20), True)
        self.user_asserts(AddNotesForTaskChecker, user_falses, Task.get(20), False)

    def test_QAForEvidenceChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4])
        self.user_asserts(QAForEvidenceChecker, user_trues, Evidence.get(2), True)
        self.user_asserts(QAForEvidenceChecker, user_falses, Evidence.get(2), False)

    def test_QAForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4])
        self.user_asserts(QAForCaseChecker, user_trues, Case.get(4), True)
        self.user_asserts(QAForCaseChecker, user_trues, Case.get(4), True)
        self.user_asserts(QAForCaseChecker, user_falses, Case.get(4), False)

    def test_AuthoriserForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [38])
        self.user_asserts(AuthoriserForCaseChecker, user_trues, Case.get(9), True)
        self.user_asserts(AuthoriserForCaseChecker, user_falses, Case.get(9), False)

    def test_AuthoriserForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [38])
        self.user_asserts(AuthoriserForTaskChecker, user_trues, Task.get(37), True)
        self.user_asserts(AuthoriserForTaskChecker, user_falses, Task.get(37), False)

    def test_AuthoriserChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [36, 37, 38, 39])
        self.user_asserts(AuthoriserChecker, user_trues, None, True)
        self.user_asserts(AuthoriserChecker, user_falses, None, False)

    def test_QAChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        self.user_asserts(QAChecker, user_trues, None, True)
        self.user_asserts(QAChecker, user_falses, None, False)

    def test_RequesterChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [26, 27, 28, 29, 30, 31, 32, 33, 34, 35])
        self.user_asserts(RequesterChecker, user_trues, None, True)
        self.user_asserts(RequesterChecker, user_falses, None, False)

    def test_RequesterExistsChecker(self):
        case_trues, case_falses = self.set_trues(self.cases, [0, 1, 3, 4, 5, 6, 7, 8, 9, 12])
        self.obj_asserts(RequesterExistsChecker, case_trues, None, True)
        self.obj_asserts(RequesterExistsChecker, case_falses, None, False)

    def test_RequesterForCaseChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [35])
        self.user_asserts(RequesterForCaseChecker, user_trues, Case.get(10), True)
        self.user_asserts(RequesterForCaseChecker, user_falses, Case.get(10), False)

    def test_RequesterForTaskChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [30])
        self.user_asserts(RequesterForTaskChecker, user_trues, Task.get(17), True)
        self.user_asserts(RequesterForTaskChecker, user_falses, Task.get(17), False)

    def test_RequesterForEvidenceChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [34])
        self.user_asserts(RequesterForEvidenceChecker, user_trues, Evidence.get(8), True)
        self.user_asserts(RequesterForEvidenceChecker, user_falses, Evidence.get(8), False)

    def test_AuthoriserForEvidenceChecker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [38])
        self.user_asserts(AuthoriserForEvidenceChecker, user_trues, Evidence.get(12), True)
        self.user_asserts(AuthoriserForEvidenceChecker, user_falses, Evidence.get(12), False)

    def test_ClosedCaseChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [2, 6])
        self.obj_asserts(ClosedCaseChecker, case_trues, None, True)
        self.obj_asserts(ClosedCaseChecker, case_falses, None, False)

    def test_DestroyedEvidenceChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.evidence, [5, 11])
        self.obj_asserts(DestroyedEvidenceChecker, case_trues, None, True)
        self.obj_asserts(DestroyedEvidenceChecker, case_falses, None, False)

    def test_ArchivedEvidenceChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.evidence, [3, 7])
        self.obj_asserts(ArchivedEvidenceChecker, case_trues, None, True)
        self.obj_asserts(ArchivedEvidenceChecker, case_falses, None, False)

    def test_ArchivedCaseChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [3, 7])
        self.obj_asserts(ArchivedCaseChecker, case_trues, None, True)
        self.obj_asserts(ArchivedCaseChecker, case_falses, None, False)

    def test_ArchivedTaskChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.tasks, [12, 13, 14, 15, 28, 29, 30, 31, 32, 33, 34])
        self.obj_asserts(ArchivedTaskChecker, case_trues, None, True)
        self.obj_asserts(ArchivedTaskChecker, case_falses, None, False)

    def test_PrivateTaskChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.tasks, [0, 1, 2, 3, 8, 9, 10, 11, 16, 22, 23, 24, 25, 26, 27, 35,
                                                              36, 37, 38, 39, 40, 41, 42, 44])
        self.obj_asserts(PrivateTaskChecker, case_trues, None, True)
        self.obj_asserts(PrivateTaskChecker, case_falses, None, False)

    def test_PrivateCaseChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [0, 2, 4, 6, 8, 10, 12])
        self.obj_asserts(PrivateCaseChecker, case_trues, None, True)
        self.obj_asserts(PrivateCaseChecker, case_falses, None, False)

    def test_PrivateEvidenceCheckerChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.evidence, [1, 3, 7, 8, 9])
        self.obj_asserts(PrivateEvidenceChecker, case_trues, None, True)
        self.obj_asserts(PrivateEvidenceChecker, case_falses, None, False)

    def test_RejectedCaseChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [9, 10])
        self.obj_asserts(RejectedCaseChecker, case_trues, None, True)
        self.obj_asserts(RejectedCaseChecker, case_falses, None, False)

    def test_NotApprovedCaseChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [4, 11])
        self.obj_asserts(NotApprovedCaseChecker, case_trues, None, True)
        self.obj_asserts(NotApprovedCaseChecker, case_falses, None, False)

    def test_CaseEditableChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [0, 1, 2, 5, 6, 8, 12])
        self.obj_asserts(CaseEditableChecker, case_trues, None, True)
        self.obj_asserts(CaseEditableChecker, case_falses, None, False)

    def test_TaskEditableChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.tasks, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17, 18, 19, 20,
                                                              21, 22, 23, 24, 25, 26, 27, 35, 36, 37, 38, 39, 40, 41,
                                                              42, 46])
        self.obj_asserts(TaskEditableChecker, case_trues, None, True)
        self.obj_asserts(TaskEditableChecker, case_falses, None, False)

    def test_EvidenceEditableChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.evidence, [0, 1, 2, 4, 8])
        self.obj_asserts(EvidenceEditableChecker, case_trues, None, True)
        self.obj_asserts(EvidenceEditableChecker, case_falses, None, False)

    def test_CaseApprovedChecker_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [0, 1, 2, 3, 5, 6, 7, 8, 12])
        self.obj_asserts(CaseApprovedChecker, case_trues, None, True)
        self.obj_asserts(CaseApprovedChecker, case_falses, None, False)

    def test_Or_test_case(self):
        case_trues, case_falses = self.set_trues(self.cases, [4, 9, 10, 11])
        self.obj_asserts(Or, case_trues, None, True, mult_checker=[RejectedCaseChecker(), NotApprovedCaseChecker()])
        self.obj_asserts(Or, case_falses, None, False, mult_checker=[RejectedCaseChecker(), NotApprovedCaseChecker()])

    def test_And_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 4, 6, 14])
        self.user_asserts(And, user_trues, None, True, mult_checker=[InvestigatorChecker(), UserIsManager()])
        self.user_asserts(And, user_falses, None, False, mult_checker=[InvestigatorChecker(), UserIsManager()])

    def test_Not_test_case(self):
        case_trues, case_falses = self.set_trues(self.tasks, [12, 13, 14, 15, 16, 28, 29, 30, 31, 32, 33, 34, 43, 44,
                                                              45])
        self.obj_asserts(Not, case_trues, None, True, mult_checker=[TaskEditableChecker()])
        self.obj_asserts(Not, case_falses, None, False, mult_checker=[TaskEditableChecker()])


    def test_UserIsManagerOfCaseWorker_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [1, 4, 6, 14, 16, 17, 18])
        self.obj_asserts(UserIsManagerOfCaseWorker, user_trues, User.get(1), True)
        self.obj_asserts(UserIsManagerOfCaseWorker, user_falses, User.get(1), False)

    def test_UserIsManagerOf_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [5, 9, 13])
        self.obj_asserts(UserIsManagerOf, user_trues, User.get(7), True)
        self.obj_asserts(UserIsManagerOf, user_falses, User.get(7), False)

    def test_UserIsManager_test_case(self):
        user_trues, user_falses = self.set_trues(self.users, [0, 1, 4, 6, 14, 16, 17, 18])
        self.user_asserts(UserIsManager, user_trues, None, True)
        self.user_asserts(UserIsManager, user_falses, None, False)

