from datetime import datetime, date
import time
from sqlalchemy import exc
# local imports
import base_tester
from foreman.model import User, Task, Department, Team, UserMessage, UserHistory, TaskTimeSheets, CaseTimeSheets, Case
from foreman.utils.utils import session


class ModelTestUserBase(base_tester.UnitTestCase):
    pass


class DepartmentTestCase(ModelTestUserBase):
    def test_department_test_case(self):
        dep = "New Department 1"

        dep_obj = Department(dep)
        session.add(dep_obj)
        session.commit()

        self.assertEqual(dep, dep_obj.department)

        session.delete(dep_obj)
        session.commit()


class TeamTestCase(ModelTestUserBase):
    def test_team_test_case(self):
        team = "New Team 1"
        dep = "New Department 2"

        dep_obj = Department(dep)
        session.add(dep_obj)
        session.commit()

        team_obj = Team(team, dep_obj)
        self.assertEqual(team, team_obj.team)
        self.assertEqual(dep, team_obj.department.department)

        teams = ["Team 2", "Team 3", "Team 4"]
        team_objs = []
        for i in range(0, 3):
            team_objs.append(Team(teams[i], dep_obj))
            session.add(team_objs[i])
            session.commit()

        self.assertEqual(4, len(dep_obj.teams))

        session.delete(dep_obj)
        session.delete(team_obj)
        for team in team_objs:
            session.delete(team)
        session.commit()


class UserMessageTestCase(ModelTestUserBase):
    def setUp(self):
        self.sender = User.get(1)
        self.receiever = User.get(2)
        self.subject = "subject"
        self.body = "body"

        self.message = UserMessage(self.sender, self.receiever, self.subject, self.body)
        session.add(self.message)
        session.commit()

    def tearDown(self):
        session.delete(self.message)
        session.commit()

    def test_messages_test_case(self):
        now = datetime.now()
        self.assertGreaterEqual(now, self.message.date_time)

        self.assertEqual(self.subject, self.message.subject)
        self.assertEqual(self.body, self.message.body)
        self.assertEqual(self.sender, self.message.sender)
        self.assertEqual(self.receiever, self.message.recipient)

        self.assertEqual(self.sender.messages_received, [])
        self.assertEqual(self.sender.messages_sent, [self.message])
        self.assertEqual(self.receiever.messages_received, [self.message])
        self.assertEqual(self.receiever.messages_sent, [])


class UserHistoryTestCase(ModelTestUserBase):
    def setUp(self):
        self.current_user = User.get(1)

        self.new_user = User("user1", "pass1", "Joe", "Bloggs", "example@example.org")
        session.add(self.new_user)
        session.commit()
        self.new_user.add_change(self.current_user)

    def tearDown(self):
        session.delete(self.new_user)
        session.commit()

    def test_history_test_case(self):
        time.sleep(1) # otherwise it happens so quickly cannot confirm that the time is different
        new_name = "Joseph"
        self.new_user.forename = new_name
        self.new_user.add_change(self.current_user)

        alls = UserHistory.get_all()
        user_hist = UserHistory.get_filter_by(original_user_id=self.new_user.id).all()
        self.assertEqual(len(user_hist), 2)

        # check histories
        result = user_hist[1].previous
        self.assertEqual(result, user_hist[0])

        result = user_hist[0].previous
        self.assertIsNone(result)

        # check changes stored
        self.assertEqual(user_hist[1].forename, new_name)
        self.assertEqual(user_hist[0].forename, "Joe")
        self.assertNotEqual(user_hist[0].date_time, user_hist[1].date_time)

        # but nothing else changed
        self.assertEqual(user_hist[0].original_user_id, user_hist[1].original_user_id)
        self.assertEqual(user_hist[0].surname, user_hist[1].surname)
        self.assertEqual(user_hist[0].username, user_hist[1].username)
        self.assertEqual(user_hist[0].photo, user_hist[1].photo)
        self.assertEqual(user_hist[0].middle, user_hist[1].middle)


class TaskTimeSheetsTestCase(ModelTestUserBase):
    def setUp(self):
        self.user = User.get(1)
        self.task = Task.get(1)
        self.date = date(2018, 1, 1)
        self.hours = 8

        self.timesheet = TaskTimeSheets(self.user, self.task, self.date, self.hours)
        session.add(self.timesheet)
        session.commit()

    def tearDown(self):
        session.delete(self.timesheet)
        session.commit()

    def test_timesheets_test_case(self):
        self.assertEqual(self.user, self.timesheet.user)
        self.assertEqual(self.task, self.timesheet.task)
        self.assertEqual(self.date, self.timesheet.date)
        self.assertEqual(self.hours, self.timesheet.hours)

        with self.assertRaises(exc.IntegrityError):
            self.timesheet.hours = 26
            try:
                session.commit()
            finally:
                session.rollback()

        with self.assertRaises(exc.IntegrityError):
            self.timesheet.hours = -8
            try:
                session.commit()
            finally:
                session.rollback()


class CaseTimeSheetsTestCase(ModelTestUserBase):
    def setUp(self):
        self.user = User.get(1)
        self.case = Case.get(1)
        self.date = date(2018, 1, 1)
        self.hours = 8

        self.timesheet = CaseTimeSheets(self.user, self.case, self.date, self.hours)
        session.add(self.timesheet)
        session.commit()

    def tearDown(self):
        session.delete(self.timesheet)
        session.commit()

    def test_case_timesheets_test_case(self):
        self.assertEqual(self.user, self.timesheet.user)
        self.assertEqual(self.case, self.timesheet.case)
        self.assertEqual(self.date, self.timesheet.date)
        self.assertEqual(self.hours, self.timesheet.hours)

        with self.assertRaises(exc.IntegrityError):
            self.timesheet.hours = 24.5
            try:
                session.commit()
            finally:
                session.rollback()

        with self.assertRaises(exc.IntegrityError):
            self.timesheet.hours = -0.2
            try:
                session.commit()
            finally:
                session.rollback()
